package mams;
/**
 * GENERATED CODE - DO NOT CHANGE
 */

import astra.core.*;
import astra.execution.*;
import astra.event.*;
import astra.messaging.*;
import astra.formula.*;
import astra.lang.*;
import astra.statement.*;
import astra.term.*;
import astra.type.*;
import astra.tr.*;
import astra.reasoner.util.*;

import mams.web.HttpRequest.HttpRequestBuilder;
import mams.web.HttpResponse;
import mams.web.HttpRequest;

public class MAMSAgent extends ASTRAClass {
	public MAMSAgent() {
		setParents(new Class[] {astra.lang.Agent.class});
		addInference(new Inference(
			new Predicate("have", new Term[] {
				new Variable(Type.STRING, "name",false)
			}),
			new Predicate("artifact", new Term[] {
				new Variable(Type.STRING, "name"),
				new Variable(Type.STRING, "qualifiedName",false),
				new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
			})
		));
		addInference(new Inference(
			new Predicate("created", new Term[] {
				new Variable(Type.STRING, "qualifiedName",false)
			}),
			new Predicate("artifact", new Term[] {
				new Variable(Type.STRING, "name",false),
				new Variable(Type.STRING, "qualifiedName"),
				new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
			})
		));
		addRule(new Rule(
			"mams.MAMSAgent", new int[] {35,9,35,19},
			new GoalEvent('+',
				new Goal(
					new Predicate("init", new Term[] {})
				)
			),
			Predicate.TRUE,
			new Block(
				"mams.MAMSAgent", new int[] {35,18,39,5},
				new Statement[] {
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {36,8,36,22},
						new Predicate("link", new Term[] {}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).link(
								);
							}
						}
					),
					new Subgoal(
						"mams.MAMSAgent", new int[] {37,8,39,5},
						new Goal(
							new Predicate("have", new Term[] {
								Primitive.newPrimitive("webserver")
							})
						)
					),
					new Subgoal(
						"mams.MAMSAgent", new int[] {38,8,39,5},
						new Goal(
							new Predicate("have", new Term[] {
								Primitive.newPrimitive("restclient")
							})
						)
					)
				}
			)
		));
		addRule(new Rule(
			"mams.MAMSAgent", new int[] {41,9,41,44},
			new GoalEvent('+',
				new Goal(
					new Predicate("have", new Term[] {
						new Variable(Type.STRING, "name",false)
					})
				)
			),
			new NOT(
				new Predicate("have", new Term[] {
					new Variable(Type.STRING, "name")
				})
			),
			new Block(
				"mams.MAMSAgent", new int[] {41,43,44,5},
				new Statement[] {
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {42,8,42,59},
						new Predicate("lookupArtifact", new Term[] {
							new Variable(Type.STRING, "name"),
							new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new BeliefUpdate('+',
						"mams.MAMSAgent", new int[] {43,8,44,5},
						new Predicate("artifact", new Term[] {
							new Variable(Type.STRING, "name"),
							new Variable(Type.STRING, "name"),
							new Variable(new ObjectType(cartago.ArtifactId.class), "id")
						})
					)
				}
			)
		));
		addRule(new Rule(
			"mams.MAMSAgent", new int[] {50,9,50,20},
			new GoalEvent('+',
				new Goal(
					new Predicate("setup", new Term[] {})
				)
			),
			Predicate.TRUE,
			new Block(
				"mams.MAMSAgent", new int[] {50,19,52,5},
				new Statement[] {
					new Subgoal(
						"mams.MAMSAgent", new int[] {51,8,52,5},
						new Goal(
							new Predicate("setup", new Term[] {
								Primitive.newPrimitive(9000)
							})
						)
					)
				}
			)
		));
		addRule(new Rule(
			"mams.MAMSAgent", new int[] {54,9,54,35},
			new GoalEvent('+',
				new Goal(
					new Predicate("setup", new Term[] {
						new Variable(Type.INTEGER, "port_number",false)
					})
				)
			),
			Predicate.TRUE,
			new Block(
				"mams.MAMSAgent", new int[] {54,34,66,5},
				new Statement[] {
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {55,8,55,30},
						new Predicate("startService", new Term[] {}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).startService(
								);
							}
						}
					),
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {56,8,56,22},
						new Predicate("link", new Term[] {}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).link(
								);
							}
						}
					),
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {58,8,58,131},
						new Predicate("makeArtifact", new Term[] {
							Primitive.newPrimitive("webserver"),
							Primitive.newPrimitive("mams.artifacts.WebServerArtifact"),
							new ModuleTerm("cartago", new ObjectType(java.lang.Object[].class),
								new Predicate("params", new Term[] {
									new ListTerm(new Term[] {
										new Variable(Type.INTEGER, "port_number")
									})
								}),
								new ModuleTermAdaptor() {
									public Object invoke(Intention intention, Predicate predicate) {
										return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).params(
											(astra.term.ListTerm) intention.evaluate(predicate.getTerm(0))
										);
									}
									public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
										return ((astra.lang.Cartago) visitor.agent().getModule("mams.MAMSAgent","cartago")).params(
											(astra.term.ListTerm) visitor.evaluate(predicate.getTerm(0))
										);
									}
								}
							),
							new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new BeliefUpdate('+',
						"mams.MAMSAgent", new int[] {59,8,66,5},
						new Predicate("artifact", new Term[] {
							Primitive.newPrimitive("webserver"),
							Primitive.newPrimitive("webserver"),
							new Variable(new ObjectType(cartago.ArtifactId.class), "id")
						})
					),
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {61,8,61,117},
						new Predicate("makeArtifact", new Term[] {
							Primitive.newPrimitive("restclient"),
							Primitive.newPrimitive("mams.artifacts.RESTArtifact"),
							new ModuleTerm("cartago", new ObjectType(java.lang.Object[].class),
								new Predicate("params", new Term[] {
									new ListTerm(new Term[] {

									})
								}),
								new ModuleTermAdaptor() {
									public Object invoke(Intention intention, Predicate predicate) {
										return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).params(
											(astra.term.ListTerm) intention.evaluate(predicate.getTerm(0))
										);
									}
									public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
										return ((astra.lang.Cartago) visitor.agent().getModule("mams.MAMSAgent","cartago")).params(
											(astra.term.ListTerm) visitor.evaluate(predicate.getTerm(0))
										);
									}
								}
							),
							new Variable(new ObjectType(cartago.ArtifactId.class), "id2",false)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new BeliefUpdate('+',
						"mams.MAMSAgent", new int[] {62,8,66,5},
						new Predicate("artifact", new Term[] {
							Primitive.newPrimitive("restclient"),
							Primitive.newPrimitive("restclient"),
							new Variable(new ObjectType(cartago.ArtifactId.class), "id2")
						})
					),
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {64,8,64,104},
						new Predicate("makeArtifact", new Term[] {
							Primitive.newPrimitive("comms"),
							Primitive.newPrimitive("fipa.artifact.Comms"),
							new ModuleTerm("cartago", new ObjectType(java.lang.Object[].class),
								new Predicate("params", new Term[] {
									new ListTerm(new Term[] {

									})
								}),
								new ModuleTermAdaptor() {
									public Object invoke(Intention intention, Predicate predicate) {
										return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).params(
											(astra.term.ListTerm) intention.evaluate(predicate.getTerm(0))
										);
									}
									public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
										return ((astra.lang.Cartago) visitor.agent().getModule("mams.MAMSAgent","cartago")).params(
											(astra.term.ListTerm) visitor.evaluate(predicate.getTerm(0))
										);
									}
								}
							),
							new Variable(new ObjectType(cartago.ArtifactId.class), "id3",false)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new BeliefUpdate('+',
						"mams.MAMSAgent", new int[] {65,8,66,5},
						new Predicate("artifact", new Term[] {
							Primitive.newPrimitive("comms"),
							Primitive.newPrimitive("comms"),
							new Variable(new ObjectType(cartago.ArtifactId.class), "id3")
						})
					)
				}
			)
		));
		addRule(new Rule(
			"mams.MAMSAgent", new int[] {72,9,73,75},
			new GoalEvent('+',
				new Goal(
					new Predicate("get", new Term[] {
						new Variable(Type.STRING, "uri",false),
						new Variable(new ObjectType(HttpResponse.class), "response",false)
					})
				)
			),
			new Predicate("artifact", new Term[] {
				Primitive.newPrimitive("restclient"),
				Primitive.newPrimitive("restclient"),
				new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
			}),
			new Block(
				"mams.MAMSAgent", new int[] {73,74,75,5},
				new Statement[] {
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {74,8,74,68},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							new Funct("execute", new Term[] {
								new ModuleTerm("httpUtils", new ObjectType(mams.web.HttpRequest.class),
									new Predicate("get", new Term[] {
										new Variable(Type.STRING, "uri")
									}),
									new ModuleTermAdaptor() {
										public Object invoke(Intention intention, Predicate predicate) {
											return ((mams.HttpUtils) intention.getModule("mams.MAMSAgent","httpUtils")).get(
												(java.lang.String) intention.evaluate(predicate.getTerm(0))
											);
										}
										public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
											return ((mams.HttpUtils) visitor.agent().getModule("mams.MAMSAgent","httpUtils")).get(
												(java.lang.String) visitor.evaluate(predicate.getTerm(0))
											);
										}
									}
								),
								new Variable(new ObjectType(HttpResponse.class), "response")
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"mams.MAMSAgent", new int[] {77,9,78,75},
			new GoalEvent('+',
				new Goal(
					new Predicate("get", new Term[] {
						new Variable(Type.STRING, "uri",false),
						new Variable(Type.LIST, "headers",false),
						new Variable(new ObjectType(HttpResponse.class), "response",false)
					})
				)
			),
			new Predicate("artifact", new Term[] {
				Primitive.newPrimitive("restclient"),
				Primitive.newPrimitive("restclient"),
				new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
			}),
			new Block(
				"mams.MAMSAgent", new int[] {78,74,80,5},
				new Statement[] {
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {79,8,79,77},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							new Funct("execute", new Term[] {
								new ModuleTerm("httpUtils", new ObjectType(mams.web.HttpRequest.class),
									new Predicate("get", new Term[] {
										new Variable(Type.STRING, "uri"),
										new Variable(Type.LIST, "headers")
									}),
									new ModuleTermAdaptor() {
										public Object invoke(Intention intention, Predicate predicate) {
											return ((mams.HttpUtils) intention.getModule("mams.MAMSAgent","httpUtils")).get(
												(java.lang.String) intention.evaluate(predicate.getTerm(0)),
												(astra.term.ListTerm) intention.evaluate(predicate.getTerm(1))
											);
										}
										public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
											return ((mams.HttpUtils) visitor.agent().getModule("mams.MAMSAgent","httpUtils")).get(
												(java.lang.String) visitor.evaluate(predicate.getTerm(0)),
												(astra.term.ListTerm) visitor.evaluate(predicate.getTerm(1))
											);
										}
									}
								),
								new Variable(new ObjectType(HttpResponse.class), "response")
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"mams.MAMSAgent", new int[] {82,9,83,75},
			new GoalEvent('+',
				new Goal(
					new Predicate("options", new Term[] {
						new Variable(Type.STRING, "uri",false),
						new Variable(new ObjectType(HttpResponse.class), "response",false)
					})
				)
			),
			new Predicate("artifact", new Term[] {
				Primitive.newPrimitive("restclient"),
				Primitive.newPrimitive("restclient"),
				new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
			}),
			new Block(
				"mams.MAMSAgent", new int[] {83,74,85,5},
				new Statement[] {
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {84,8,84,72},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							new Funct("execute", new Term[] {
								new ModuleTerm("httpUtils", new ObjectType(mams.web.HttpRequest.class),
									new Predicate("options", new Term[] {
										new Variable(Type.STRING, "uri")
									}),
									new ModuleTermAdaptor() {
										public Object invoke(Intention intention, Predicate predicate) {
											return ((mams.HttpUtils) intention.getModule("mams.MAMSAgent","httpUtils")).options(
												(java.lang.String) intention.evaluate(predicate.getTerm(0))
											);
										}
										public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
											return ((mams.HttpUtils) visitor.agent().getModule("mams.MAMSAgent","httpUtils")).options(
												(java.lang.String) visitor.evaluate(predicate.getTerm(0))
											);
										}
									}
								),
								new Variable(new ObjectType(HttpResponse.class), "response")
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"mams.MAMSAgent", new int[] {87,9,88,75},
			new GoalEvent('+',
				new Goal(
					new Predicate("head", new Term[] {
						new Variable(Type.STRING, "uri",false),
						new Variable(new ObjectType(HttpResponse.class), "response",false)
					})
				)
			),
			new Predicate("artifact", new Term[] {
				Primitive.newPrimitive("restclient"),
				Primitive.newPrimitive("restclient"),
				new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
			}),
			new Block(
				"mams.MAMSAgent", new int[] {88,74,90,5},
				new Statement[] {
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {89,8,89,69},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							new Funct("execute", new Term[] {
								new ModuleTerm("httpUtils", new ObjectType(mams.web.HttpRequest.class),
									new Predicate("head", new Term[] {
										new Variable(Type.STRING, "uri")
									}),
									new ModuleTermAdaptor() {
										public Object invoke(Intention intention, Predicate predicate) {
											return ((mams.HttpUtils) intention.getModule("mams.MAMSAgent","httpUtils")).head(
												(java.lang.String) intention.evaluate(predicate.getTerm(0))
											);
										}
										public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
											return ((mams.HttpUtils) visitor.agent().getModule("mams.MAMSAgent","httpUtils")).head(
												(java.lang.String) visitor.evaluate(predicate.getTerm(0))
											);
										}
									}
								),
								new Variable(new ObjectType(HttpResponse.class), "response")
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"mams.MAMSAgent", new int[] {93,9,94,75},
			new GoalEvent('+',
				new Goal(
					new Predicate("post", new Term[] {
						new Variable(Type.STRING, "uri",false),
						new Variable(Type.STRING, "jsonBody",false),
						new Variable(new ObjectType(HttpResponse.class), "response",false)
					})
				)
			),
			new Predicate("artifact", new Term[] {
				Primitive.newPrimitive("restclient"),
				Primitive.newPrimitive("restclient"),
				new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
			}),
			new Block(
				"mams.MAMSAgent", new int[] {94,74,98,5},
				new Statement[] {
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {95,8,97,9},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							new Funct("execute", new Term[] {
								new ModuleTerm("httpUtils", new ObjectType(mams.web.HttpRequest.class),
									new Predicate("post", new Term[] {
										new Variable(Type.STRING, "uri"),
										new ListTerm(new Term[] {
											new Funct("header", new Term[] {
												Primitive.newPrimitive("Content-Type"),
												Primitive.newPrimitive("application/json")
											})
										}),
										new Variable(Type.STRING, "jsonBody")
									}),
									new ModuleTermAdaptor() {
										public Object invoke(Intention intention, Predicate predicate) {
											return ((mams.HttpUtils) intention.getModule("mams.MAMSAgent","httpUtils")).post(
												(java.lang.String) intention.evaluate(predicate.getTerm(0)),
												(astra.term.ListTerm) intention.evaluate(predicate.getTerm(1)),
												(java.lang.String) intention.evaluate(predicate.getTerm(2))
											);
										}
										public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
											return ((mams.HttpUtils) visitor.agent().getModule("mams.MAMSAgent","httpUtils")).post(
												(java.lang.String) visitor.evaluate(predicate.getTerm(0)),
												(astra.term.ListTerm) visitor.evaluate(predicate.getTerm(1)),
												(java.lang.String) visitor.evaluate(predicate.getTerm(2))
											);
										}
									}
								),
								new Variable(new ObjectType(HttpResponse.class), "response")
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"mams.MAMSAgent", new int[] {100,9,101,75},
			new GoalEvent('+',
				new Goal(
					new Predicate("patch", new Term[] {
						new Variable(Type.STRING, "uri",false),
						new Variable(Type.STRING, "jsonBody",false),
						new Variable(new ObjectType(HttpResponse.class), "response",false)
					})
				)
			),
			new Predicate("artifact", new Term[] {
				Primitive.newPrimitive("restclient"),
				Primitive.newPrimitive("restclient"),
				new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
			}),
			new Block(
				"mams.MAMSAgent", new int[] {101,74,105,5},
				new Statement[] {
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {102,8,104,9},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							new Funct("execute", new Term[] {
								new ModuleTerm("httpUtils", new ObjectType(mams.web.HttpRequest.class),
									new Predicate("patch", new Term[] {
										new Variable(Type.STRING, "uri"),
										new ListTerm(new Term[] {
											new Funct("header", new Term[] {
												Primitive.newPrimitive("Content-Type"),
												Primitive.newPrimitive("application/json")
											})
										}),
										new Variable(Type.STRING, "jsonBody")
									}),
									new ModuleTermAdaptor() {
										public Object invoke(Intention intention, Predicate predicate) {
											return ((mams.HttpUtils) intention.getModule("mams.MAMSAgent","httpUtils")).patch(
												(java.lang.String) intention.evaluate(predicate.getTerm(0)),
												(astra.term.ListTerm) intention.evaluate(predicate.getTerm(1)),
												(java.lang.String) intention.evaluate(predicate.getTerm(2))
											);
										}
										public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
											return ((mams.HttpUtils) visitor.agent().getModule("mams.MAMSAgent","httpUtils")).patch(
												(java.lang.String) visitor.evaluate(predicate.getTerm(0)),
												(astra.term.ListTerm) visitor.evaluate(predicate.getTerm(1)),
												(java.lang.String) visitor.evaluate(predicate.getTerm(2))
											);
										}
									}
								),
								new Variable(new ObjectType(HttpResponse.class), "response")
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"mams.MAMSAgent", new int[] {107,9,108,75},
			new GoalEvent('+',
				new Goal(
					new Predicate("put", new Term[] {
						new Variable(Type.STRING, "uri",false),
						new Variable(Type.STRING, "jsonBody",false),
						new Variable(new ObjectType(HttpResponse.class), "response",false)
					})
				)
			),
			new Predicate("artifact", new Term[] {
				Primitive.newPrimitive("restclient"),
				Primitive.newPrimitive("restclient"),
				new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
			}),
			new Block(
				"mams.MAMSAgent", new int[] {108,74,112,5},
				new Statement[] {
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {109,8,111,9},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							new Funct("execute", new Term[] {
								new ModuleTerm("httpUtils", new ObjectType(mams.web.HttpRequest.class),
									new Predicate("put", new Term[] {
										new Variable(Type.STRING, "uri"),
										new ListTerm(new Term[] {
											new Funct("header", new Term[] {
												Primitive.newPrimitive("Content-Type"),
												Primitive.newPrimitive("application/json")
											})
										}),
										new Variable(Type.STRING, "jsonBody")
									}),
									new ModuleTermAdaptor() {
										public Object invoke(Intention intention, Predicate predicate) {
											return ((mams.HttpUtils) intention.getModule("mams.MAMSAgent","httpUtils")).put(
												(java.lang.String) intention.evaluate(predicate.getTerm(0)),
												(astra.term.ListTerm) intention.evaluate(predicate.getTerm(1)),
												(java.lang.String) intention.evaluate(predicate.getTerm(2))
											);
										}
										public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
											return ((mams.HttpUtils) visitor.agent().getModule("mams.MAMSAgent","httpUtils")).put(
												(java.lang.String) visitor.evaluate(predicate.getTerm(0)),
												(astra.term.ListTerm) visitor.evaluate(predicate.getTerm(1)),
												(java.lang.String) visitor.evaluate(predicate.getTerm(2))
											);
										}
									}
								),
								new Variable(new ObjectType(HttpResponse.class), "response")
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"mams.MAMSAgent", new int[] {114,9,115,75},
			new GoalEvent('+',
				new Goal(
					new Predicate("delete", new Term[] {
						new Variable(Type.STRING, "uri",false),
						new Variable(new ObjectType(HttpResponse.class), "response",false)
					})
				)
			),
			new Predicate("artifact", new Term[] {
				Primitive.newPrimitive("restclient"),
				Primitive.newPrimitive("restclient"),
				new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
			}),
			new Block(
				"mams.MAMSAgent", new int[] {115,74,117,5},
				new Statement[] {
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {116,8,116,71},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							new Funct("execute", new Term[] {
								new ModuleTerm("httpUtils", new ObjectType(mams.web.HttpRequest.class),
									new Predicate("delete", new Term[] {
										new Variable(Type.STRING, "uri")
									}),
									new ModuleTermAdaptor() {
										public Object invoke(Intention intention, Predicate predicate) {
											return ((mams.HttpUtils) intention.getModule("mams.MAMSAgent","httpUtils")).delete(
												(java.lang.String) intention.evaluate(predicate.getTerm(0))
											);
										}
										public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
											return ((mams.HttpUtils) visitor.agent().getModule("mams.MAMSAgent","httpUtils")).delete(
												(java.lang.String) visitor.evaluate(predicate.getTerm(0))
											);
										}
									}
								),
								new Variable(new ObjectType(HttpResponse.class), "response")
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"mams.MAMSAgent", new int[] {120,9,121,75},
			new GoalEvent('+',
				new Goal(
					new Predicate("httpRequest", new Term[] {
						new Variable(new ObjectType(HttpRequest.class), "httpRequest",false),
						new Variable(new ObjectType(HttpResponse.class), "repsonse",false)
					})
				)
			),
			new Predicate("artifact", new Term[] {
				Primitive.newPrimitive("restclient"),
				Primitive.newPrimitive("restclient"),
				new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
			}),
			new Block(
				"mams.MAMSAgent", new int[] {121,74,123,5},
				new Statement[] {
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {122,8,122,61},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							new Funct("execute", new Term[] {
								new Variable(new ObjectType(HttpRequest.class), "httpRequest"),
								new Variable(new ObjectType(HttpResponse.class), "repsonse")
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"mams.MAMSAgent", new int[] {129,9,130,75},
			new GoalEvent('+',
				new Goal(
					new Predicate("post", new Term[] {
						new Variable(Type.STRING, "uri",false),
						new Variable(Type.STRING, "jsonBody",false),
						new Variable(Type.INTEGER, "responseCode",false),
						new Variable(Type.STRING, "content",false)
					})
				)
			),
			new Predicate("artifact", new Term[] {
				Primitive.newPrimitive("restclient"),
				Primitive.newPrimitive("restclient"),
				new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
			}),
			new Block(
				"mams.MAMSAgent", new int[] {130,74,132,5},
				new Statement[] {
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {131,8,131,100},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							new Funct("postRequest", new Term[] {
								new Variable(Type.STRING, "uri"),
								new Variable(Type.STRING, "jsonBody"),
								Primitive.newPrimitive("application/json"),
								new Variable(Type.INTEGER, "responseCode"),
								new Variable(Type.STRING, "content")
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"mams.MAMSAgent", new int[] {134,9,135,75},
			new GoalEvent('+',
				new Goal(
					new Predicate("post", new Term[] {
						new Variable(Type.STRING, "uri",false),
						new Variable(Type.STRING, "jsonBody",false),
						new Variable(Type.STRING, "mediaType",false),
						new Variable(Type.INTEGER, "responseCode",false),
						new Variable(Type.STRING, "content",false)
					})
				)
			),
			new Predicate("artifact", new Term[] {
				Primitive.newPrimitive("restclient"),
				Primitive.newPrimitive("restclient"),
				new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
			}),
			new Block(
				"mams.MAMSAgent", new int[] {135,74,137,5},
				new Statement[] {
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {136,8,136,91},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							new Funct("postRequest", new Term[] {
								new Variable(Type.STRING, "uri"),
								new Variable(Type.STRING, "jsonBody"),
								new Variable(Type.STRING, "mediaType"),
								new Variable(Type.INTEGER, "responseCode"),
								new Variable(Type.STRING, "content")
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"mams.MAMSAgent", new int[] {139,9,140,75},
			new GoalEvent('+',
				new Goal(
					new Predicate("put", new Term[] {
						new Variable(Type.STRING, "uri",false),
						new Variable(Type.STRING, "jsonBody",false),
						new Variable(Type.INTEGER, "responseCode",false),
						new Variable(Type.STRING, "content",false)
					})
				)
			),
			new Predicate("artifact", new Term[] {
				Primitive.newPrimitive("restclient"),
				Primitive.newPrimitive("restclient"),
				new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
			}),
			new Block(
				"mams.MAMSAgent", new int[] {140,74,142,5},
				new Statement[] {
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {141,8,141,99},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							new Funct("putRequest", new Term[] {
								new Variable(Type.STRING, "uri"),
								new Variable(Type.STRING, "jsonBody"),
								Primitive.newPrimitive("application/json"),
								new Variable(Type.INTEGER, "responseCode"),
								new Variable(Type.STRING, "content")
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"mams.MAMSAgent", new int[] {144,9,145,75},
			new GoalEvent('+',
				new Goal(
					new Predicate("put", new Term[] {
						new Variable(Type.STRING, "uri",false),
						new Variable(Type.STRING, "jsonBody",false),
						new Variable(Type.STRING, "mediaType",false),
						new Variable(Type.INTEGER, "responseCode",false),
						new Variable(Type.STRING, "content",false)
					})
				)
			),
			new Predicate("artifact", new Term[] {
				Primitive.newPrimitive("restclient"),
				Primitive.newPrimitive("restclient"),
				new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
			}),
			new Block(
				"mams.MAMSAgent", new int[] {145,74,147,5},
				new Statement[] {
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {146,8,146,90},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							new Funct("putRequest", new Term[] {
								new Variable(Type.STRING, "uri"),
								new Variable(Type.STRING, "jsonBody"),
								new Variable(Type.STRING, "mediaType"),
								new Variable(Type.INTEGER, "responseCode"),
								new Variable(Type.STRING, "content")
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"mams.MAMSAgent", new int[] {149,9,150,75},
			new GoalEvent('+',
				new Goal(
					new Predicate("get", new Term[] {
						new Variable(Type.STRING, "uri",false),
						new Variable(Type.INTEGER, "responseCode",false),
						new Variable(Type.STRING, "content",false)
					})
				)
			),
			new Predicate("artifact", new Term[] {
				Primitive.newPrimitive("restclient"),
				Primitive.newPrimitive("restclient"),
				new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
			}),
			new Block(
				"mams.MAMSAgent", new int[] {150,74,152,5},
				new Statement[] {
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {151,8,151,89},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							new Funct("getRequest", new Term[] {
								new Variable(Type.STRING, "uri"),
								Primitive.newPrimitive("application/json"),
								new Variable(Type.INTEGER, "responseCode"),
								new Variable(Type.STRING, "content")
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"mams.MAMSAgent", new int[] {154,9,155,75},
			new GoalEvent('+',
				new Goal(
					new Predicate("get", new Term[] {
						new Variable(Type.STRING, "uri",false),
						new Variable(Type.STRING, "mediaType",false),
						new Variable(Type.INTEGER, "responseCode",false),
						new Variable(Type.STRING, "content",false)
					})
				)
			),
			new Predicate("artifact", new Term[] {
				Primitive.newPrimitive("restclient"),
				Primitive.newPrimitive("restclient"),
				new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
			}),
			new Block(
				"mams.MAMSAgent", new int[] {155,74,157,5},
				new Statement[] {
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {156,8,156,80},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							new Funct("getRequest", new Term[] {
								new Variable(Type.STRING, "uri"),
								new Variable(Type.STRING, "mediaType"),
								new Variable(Type.INTEGER, "responseCode"),
								new Variable(Type.STRING, "content")
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"mams.MAMSAgent", new int[] {162,9,162,117},
			new GoalEvent('+',
				new Goal(
					new Predicate("created", new Term[] {
						Primitive.newPrimitive("base")
					})
				)
			),
			new AND(
				new NOT(
					new Predicate("created", new Term[] {
						Primitive.newPrimitive("base")
					})
				),
				new Predicate("artifact", new Term[] {
					Primitive.newPrimitive("webserver"),
					new Variable(Type.STRING, "qualifiedName",false),
					new Variable(new ObjectType(cartago.ArtifactId.class), "id2",false)
				})
			),
			new Block(
				"mams.MAMSAgent", new int[] {162,116,171,5},
				new Statement[] {
					new Declaration(
						new Variable(Type.STRING, "baseName"),
						"mams.MAMSAgent", new int[] {163,8,171,5},
						Operator.newOperator('+',
							new ModuleTerm("S", Type.STRING,
								new Predicate("name", new Term[] {}),
								new ModuleTermAdaptor() {
									public Object invoke(Intention intention, Predicate predicate) {
										return ((astra.lang.System) intention.getModule("mams.MAMSAgent","S")).name(
										);
									}
									public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
										return ((astra.lang.System) visitor.agent().getModule("mams.MAMSAgent","S")).name(
										);
									}
								}
							),
							Primitive.newPrimitive("-base")
						)
					),
					new ModuleCall("console",
						"mams.MAMSAgent", new int[] {164,8,164,48},
						new Predicate("println", new Term[] {
							Operator.newOperator('+',
								Primitive.newPrimitive("creating: "),
								new Variable(Type.STRING, "baseName")
							)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Console) intention.getModule("mams.MAMSAgent","console")).println(
									(java.lang.String) intention.evaluate(predicate.getTerm(0))
								);
							}
						}
					),
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {165,8,165,120},
						new Predicate("makeArtifact", new Term[] {
							new Variable(Type.STRING, "baseName"),
							Primitive.newPrimitive("mams.artifacts.BaseArtifact"),
							new ModuleTerm("cartago", new ObjectType(java.lang.Object[].class),
								new Predicate("params", new Term[] {
									new ListTerm(new Term[] {
										new ModuleTerm("S", Type.STRING,
											new Predicate("name", new Term[] {}),
											new ModuleTermAdaptor() {
												public Object invoke(Intention intention, Predicate predicate) {
													return ((astra.lang.System) intention.getModule("mams.MAMSAgent","S")).name(
													);
												}
												public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
													return ((astra.lang.System) visitor.agent().getModule("mams.MAMSAgent","S")).name(
													);
												}
											}
										)
									})
								}),
								new ModuleTermAdaptor() {
									public Object invoke(Intention intention, Predicate predicate) {
										return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).params(
											(astra.term.ListTerm) intention.evaluate(predicate.getTerm(0))
										);
									}
									public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
										return ((astra.lang.Cartago) visitor.agent().getModule("mams.MAMSAgent","cartago")).params(
											(astra.term.ListTerm) visitor.evaluate(predicate.getTerm(0))
										);
									}
								}
							),
							new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {166,8,166,47},
						new Predicate("linkArtifacts", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							Primitive.newPrimitive("out-1"),
							new Variable(new ObjectType(cartago.ArtifactId.class), "id2")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {167,8,167,25},
						new Predicate("focus", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {168,8,168,44},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							new Funct("createRoute", new Term[] {})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new BeliefUpdate('+',
						"mams.MAMSAgent", new int[] {169,8,171,5},
						new Predicate("artifact", new Term[] {
							Primitive.newPrimitive("base"),
							new Variable(Type.STRING, "baseName"),
							new Variable(new ObjectType(cartago.ArtifactId.class), "id")
						})
					),
					new BeliefUpdate('+',
						"mams.MAMSAgent", new int[] {170,8,171,5},
						new Predicate("created", new Term[] {
							Primitive.newPrimitive("base")
						})
					)
				}
			)
		));
		addRule(new Rule(
			"mams.MAMSAgent", new int[] {173,9,173,99},
			new GoalEvent('+',
				new Goal(
					new Predicate("created", new Term[] {
						Primitive.newPrimitive("base")
					})
				)
			),
			new NOT(
				new Predicate("artifact", new Term[] {
					Primitive.newPrimitive("webserver"),
					new Variable(Type.STRING, "qualifiedName",false),
					new Variable(new ObjectType(cartago.ArtifactId.class), "id2",false)
				})
			),
			new Block(
				"mams.MAMSAgent", new int[] {173,98,176,5},
				new Statement[] {
					new ModuleCall("console",
						"mams.MAMSAgent", new int[] {174,8,174,76},
						new Predicate("println", new Term[] {
							Primitive.newPrimitive("Attempt to create base, but agent Not Initialized")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Console) intention.getModule("mams.MAMSAgent","console")).println(
									(java.lang.String) intention.evaluate(predicate.getTerm(0))
								);
							}
						}
					),
					new ModuleCall("S",
						"mams.MAMSAgent", new int[] {175,8,175,16},
						new Predicate("exit", new Term[] {}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.System) intention.getModule("mams.MAMSAgent","S")).exit(
								);
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"mams.MAMSAgent", new int[] {178,9,178,53},
			new GoalEvent('+',
				new Goal(
					new Predicate("getAgentURI", new Term[] {
						new Variable(Type.STRING, "uri",false)
					})
				)
			),
			new Predicate("created", new Term[] {
				Primitive.newPrimitive("base")
			}),
			new Block(
				"mams.MAMSAgent", new int[] {178,52,182,5},
				new Statement[] {
					new Subgoal(
						"mams.MAMSAgent", new int[] {179,8,182,5},
						new Goal(
							new Predicate("itemProperty", new Term[] {
								Primitive.newPrimitive("base"),
								Primitive.newPrimitive("uri"),
								new Variable(Type.FUNCTION, "agentUri",false)
							})
						)
					),
					new Assignment(
						new Variable(Type.STRING, "uri"),
						"mams.MAMSAgent", new int[] {180,8,182,5},
						new ModuleTerm("F", Type.STRING,
							new Predicate("valueAsString", new Term[] {
								new Variable(Type.FUNCTION, "agentUri"),
								Primitive.newPrimitive(0)
							}),
							new ModuleTermAdaptor() {
								public Object invoke(Intention intention, Predicate predicate) {
									return ((astra.lang.Functions) intention.getModule("mams.MAMSAgent","F")).valueAsString(
										(astra.term.Funct) intention.evaluate(predicate.getTerm(0)),
										(int) intention.evaluate(predicate.getTerm(1))
									);
								}
								public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
									return ((astra.lang.Functions) visitor.agent().getModule("mams.MAMSAgent","F")).valueAsString(
										(astra.term.Funct) visitor.evaluate(predicate.getTerm(0)),
										(int) visitor.evaluate(predicate.getTerm(1))
									);
								}
							}
						)
					),
					new ModuleCall("console",
						"mams.MAMSAgent", new int[] {181,8,181,45},
						new Predicate("println", new Term[] {
							Operator.newOperator('+',
								Primitive.newPrimitive("getAgentURI: "),
								new Variable(Type.STRING, "uri")
							)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Console) intention.getModule("mams.MAMSAgent","console")).println(
									(java.lang.String) intention.evaluate(predicate.getTerm(0))
								);
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"mams.MAMSAgent", new int[] {187,9,187,111},
			new GoalEvent('+',
				new Goal(
					new Predicate("created", new Term[] {
						Primitive.newPrimitive("inbox")
					})
				)
			),
			new AND(
				new NOT(
					new Predicate("have", new Term[] {
						Primitive.newPrimitive("inbox")
					})
				),
				new Predicate("artifact", new Term[] {
					Primitive.newPrimitive("base"),
					new Variable(Type.STRING, "qualifiedName",false),
					new Variable(new ObjectType(cartago.ArtifactId.class), "id2",false)
				})
			),
			new Block(
				"mams.MAMSAgent", new int[] {187,110,196,5},
				new Statement[] {
					new Declaration(
						new Variable(Type.STRING, "baseName"),
						"mams.MAMSAgent", new int[] {188,8,196,5},
						Operator.newOperator('+',
							new Variable(Type.STRING, "qualifiedName"),
							Primitive.newPrimitive("-inbox")
						)
					),
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {190,8,190,104},
						new Predicate("makeArtifact", new Term[] {
							new Variable(Type.STRING, "baseName"),
							Primitive.newPrimitive("fipa.artifact.Inbox"),
							new ModuleTerm("cartago", new ObjectType(java.lang.Object[].class),
								new Predicate("params", new Term[] {
									new ListTerm(new Term[] {

									})
								}),
								new ModuleTermAdaptor() {
									public Object invoke(Intention intention, Predicate predicate) {
										return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).params(
											(astra.term.ListTerm) intention.evaluate(predicate.getTerm(0))
										);
									}
									public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
										return ((astra.lang.Cartago) visitor.agent().getModule("mams.MAMSAgent","cartago")).params(
											(astra.term.ListTerm) visitor.evaluate(predicate.getTerm(0))
										);
									}
								}
							),
							new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {191,8,191,47},
						new Predicate("linkArtifacts", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							Primitive.newPrimitive("out-1"),
							new Variable(new ObjectType(cartago.ArtifactId.class), "id2")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {192,8,192,25},
						new Predicate("focus", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {193,8,193,44},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							new Funct("createRoute", new Term[] {})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new BeliefUpdate('+',
						"mams.MAMSAgent", new int[] {195,8,196,5},
						new Predicate("artifact", new Term[] {
							Primitive.newPrimitive("inbox"),
							new Variable(Type.STRING, "baseName"),
							new Variable(new ObjectType(cartago.ArtifactId.class), "id")
						})
					)
				}
			)
		));
		addRule(new Rule(
			"mams.MAMSAgent", new int[] {198,9,198,98},
			new ModuleEvent("cartago",
				"$cse",
				new Predicate("signal", new Term[] {
					new Variable(Type.STRING, "sa",false),
					new Funct("message", new Term[] {
						new Variable(Type.STRING, "performative",false),
						new Variable(Type.STRING, "sender",false),
						new Variable(Type.STRING, "content",false)
					})
				}),
				new ModuleEventAdaptor() {
					public Event generate(astra.core.Agent agent, Predicate predicate) {
						return ((astra.lang.Cartago) agent.getModule("mams.MAMSAgent","cartago")).signal(
							predicate.getTerm(0),
							predicate.getTerm(1)
						);
					}
				}
			),
			Predicate.TRUE,
			new Block(
				"mams.MAMSAgent", new int[] {198,97,200,5},
				new Statement[] {
					new ScopedSubgoal(
						"mams.MAMSAgent", new int[] {199,8,200,5},
						"MAMSAgent",
						new Goal(
							new Predicate("signal_message", new Term[] {
								new Variable(Type.STRING, "performative"),
								new Variable(Type.STRING, "sender"),
								new ModuleTerm("converter", Type.FUNCTION,
									new Predicate("toRawFunct", new Term[] {
										Primitive.newPrimitive("content"),
										new Variable(Type.STRING, "content")
									}),
									new ModuleTermAdaptor() {
										public Object invoke(Intention intention, Predicate predicate) {
											return ((mams.JSONConverter) intention.getModule("mams.MAMSAgent","converter")).toRawFunct(
												(java.lang.String) intention.evaluate(predicate.getTerm(0)),
												(java.lang.String) intention.evaluate(predicate.getTerm(1))
											);
										}
										public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
											return ((mams.JSONConverter) visitor.agent().getModule("mams.MAMSAgent","converter")).toRawFunct(
												(java.lang.String) visitor.evaluate(predicate.getTerm(0)),
												(java.lang.String) visitor.evaluate(predicate.getTerm(1))
											);
										}
									}
								)
							})
						)
					)
				}
			)
		));
		addRule(new Rule(
			"mams.MAMSAgent", new int[] {202,9,202,87},
			new GoalEvent('+',
				new Goal(
					new Predicate("signal_message", new Term[] {
						new Variable(Type.STRING, "performative",false),
						new Variable(Type.STRING, "sender",false),
						new Funct("content", new Term[] {
							new Variable(Type.FUNCTION, "content",false)
						})
					})
				)
			),
			Predicate.TRUE,
			new Block(
				"mams.MAMSAgent", new int[] {202,86,204,5},
				new Statement[] {
					new Subgoal(
						"mams.MAMSAgent", new int[] {203,8,204,5},
						new Goal(
							new Predicate("message", new Term[] {
								new Variable(Type.STRING, "performative"),
								new Variable(Type.STRING, "sender"),
								new Variable(Type.FUNCTION, "content")
							})
						)
					)
				}
			)
		));
		addRule(new Rule(
			"mams.MAMSAgent", new int[] {206,9,206,139},
			new GoalEvent('+',
				new Goal(
					new Predicate("transmit", new Term[] {
						new Variable(Type.STRING, "performative",false),
						new Variable(Type.STRING, "receiver",false),
						new Variable(Type.FUNCTION, "content",false)
					})
				)
			),
			new Predicate("artifact", new Term[] {
				Primitive.newPrimitive("comms"),
				new Variable(Type.STRING, "qualifiedName",false),
				new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
			}),
			new Block(
				"mams.MAMSAgent", new int[] {206,138,209,5},
				new Statement[] {
					new Subgoal(
						"mams.MAMSAgent", new int[] {207,8,209,5},
						new Goal(
							new Predicate("itemProperty", new Term[] {
								Primitive.newPrimitive("base"),
								Primitive.newPrimitive("uri"),
								new Variable(Type.FUNCTION, "agentUri",false)
							})
						)
					),
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {208,8,208,134},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							new Funct("transmit", new Term[] {
								new Variable(Type.STRING, "performative"),
								new ModuleTerm("F", Type.STRING,
									new Predicate("valueAsString", new Term[] {
										new Variable(Type.FUNCTION, "agentUri"),
										Primitive.newPrimitive(0)
									}),
									new ModuleTermAdaptor() {
										public Object invoke(Intention intention, Predicate predicate) {
											return ((astra.lang.Functions) intention.getModule("mams.MAMSAgent","F")).valueAsString(
												(astra.term.Funct) intention.evaluate(predicate.getTerm(0)),
												(int) intention.evaluate(predicate.getTerm(1))
											);
										}
										public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
											return ((astra.lang.Functions) visitor.agent().getModule("mams.MAMSAgent","F")).valueAsString(
												(astra.term.Funct) visitor.evaluate(predicate.getTerm(0)),
												(int) visitor.evaluate(predicate.getTerm(1))
											);
										}
									}
								),
								new Variable(Type.STRING, "receiver"),
								new ModuleTerm("converter", Type.STRING,
									new Predicate("toJsonString", new Term[] {
										new Funct("content", new Term[] {
											new Variable(Type.FUNCTION, "content")
										})
									}),
									new ModuleTermAdaptor() {
										public Object invoke(Intention intention, Predicate predicate) {
											return ((mams.JSONConverter) intention.getModule("mams.MAMSAgent","converter")).toJsonString(
												(astra.term.Funct) intention.evaluate(predicate.getTerm(0))
											);
										}
										public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
											return ((mams.JSONConverter) visitor.agent().getModule("mams.MAMSAgent","converter")).toJsonString(
												(astra.term.Funct) visitor.evaluate(predicate.getTerm(0))
											);
										}
									}
								)
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"mams.MAMSAgent", new int[] {214,9,214,62},
			new GoalEvent('+',
				new Goal(
					new Predicate("schema", new Term[] {
						new Variable(Type.STRING, "schema",false)
					})
				)
			),
			new NOT(
				new Predicate("schema", new Term[] {
					new Variable(Type.STRING, "schema"),
					new Variable(Type.STRING, "X",false)
				})
			),
			new Block(
				"mams.MAMSAgent", new int[] {214,61,219,5},
				new Statement[] {
					new Declaration(
						new Variable(Type.LIST, "fields"),
						"mams.MAMSAgent", new int[] {215,8,219,5},
						new ModuleTerm("converter", Type.LIST,
							new Predicate("getFields", new Term[] {
								new Variable(Type.STRING, "schema")
							}),
							new ModuleTermAdaptor() {
								public Object invoke(Intention intention, Predicate predicate) {
									return ((mams.JSONConverter) intention.getModule("mams.MAMSAgent","converter")).getFields(
										(java.lang.String) intention.evaluate(predicate.getTerm(0))
									);
								}
								public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
									return ((mams.JSONConverter) visitor.agent().getModule("mams.MAMSAgent","converter")).getFields(
										(java.lang.String) visitor.evaluate(predicate.getTerm(0))
									);
								}
							}
						)
					),
					new ForAll(
						"mams.MAMSAgent", new int[] {216,8,216,38},
						new Variable(Type.STRING, "field",false),
						new Variable(Type.LIST, "fields"),
						new Block(
							"mams.MAMSAgent", new int[] {216,39,219,5},
							new Statement[] {
								new BeliefUpdate('+',
									"mams.MAMSAgent", new int[] {217,12,218,9},
									new Predicate("schema", new Term[] {
										new Variable(Type.STRING, "schema"),
										new Variable(Type.STRING, "field")
									})
								)
							}
						)
					)
				}
			)
		));
		addRule(new Rule(
			"mams.MAMSAgent", new int[] {224,9,224,141},
			new GoalEvent('+',
				new Goal(
					new Predicate("itemProperty", new Term[] {
						new Variable(Type.STRING, "artifact_name",false),
						new Variable(Type.STRING, "property",false),
						new Variable(Type.FUNCTION, "value",false)
					})
				)
			),
			new Predicate("artifact", new Term[] {
				new Variable(Type.STRING, "artifact_name"),
				new Variable(Type.STRING, "qname",false),
				new Variable(new ObjectType(cartago.ArtifactId.class), "aid",false)
			}),
			new Block(
				"mams.MAMSAgent", new int[] {224,140,227,5},
				new Statement[] {
					new ModuleCall("cartago",
						"mams.MAMSAgent", new int[] {225,8,225,91},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "aid"),
							new Funct("observeProperty", new Term[] {
								new Variable(Type.STRING, "property"),
								new Variable(new ObjectType(cartago.ArtifactObsProperty.class), "prop",false)
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new Assignment(
						new Variable(Type.FUNCTION, "value"),
						"mams.MAMSAgent", new int[] {226,8,227,5},
						new ModuleTerm("cartago", Type.FUNCTION,
							new Predicate("toFunction", new Term[] {
								new Variable(new ObjectType(cartago.ArtifactObsProperty.class), "prop")
							}),
							new ModuleTermAdaptor() {
								public Object invoke(Intention intention, Predicate predicate) {
									return ((astra.lang.Cartago) intention.getModule("mams.MAMSAgent","cartago")).toFunction(
										(cartago.ArtifactObsProperty) intention.evaluate(predicate.getTerm(0))
									);
								}
								public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
									return ((astra.lang.Cartago) visitor.agent().getModule("mams.MAMSAgent","cartago")).toFunction(
										(cartago.ArtifactObsProperty) visitor.evaluate(predicate.getTerm(0))
									);
								}
							}
						)
					)
				}
			)
		));
	}

	public void initialize(astra.core.Agent agent) {
	}

	public Fragment createFragment(astra.core.Agent agent) throws ASTRAClassNotFoundException {
		Fragment fragment = new Fragment(this);
		fragment.addModule("cartago",astra.lang.Cartago.class,agent);
		fragment.addModule("console",astra.lang.Console.class,agent);
		fragment.addModule("S",astra.lang.System.class,agent);
		fragment.addModule("F",astra.lang.Functions.class,agent);
		fragment.addModule("httpUtils",mams.HttpUtils.class,agent);
		fragment.addModule("converter",mams.JSONConverter.class,agent);
		return fragment;
	}

	public static void main(String[] args) {
		Scheduler.setStrategy(new TestSchedulerStrategy());
		ListTerm argList = new ListTerm();
		for (String arg: args) {
			argList.add(Primitive.newPrimitive(arg));
		}

		String name = java.lang.System.getProperty("astra.name", "main");
		try {
			astra.core.Agent agent = new MAMSAgent().newInstance(name);
			agent.initialize(new Goal(new Predicate("main", new Term[] { argList })));
			Scheduler.schedule(agent);
		} catch (AgentCreationException e) {
			e.printStackTrace();
		} catch (ASTRAClassNotFoundException e) {
			e.printStackTrace();
		};
	}
}
