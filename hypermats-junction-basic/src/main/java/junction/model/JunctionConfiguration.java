package junction.model;

public class JunctionConfiguration {
    public String id;
    public int inLinks;
    public int outLinks;
}
