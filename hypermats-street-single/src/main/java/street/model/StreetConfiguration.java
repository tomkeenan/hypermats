package street.model;

public class StreetConfiguration {
    public StreetConfiguration(String id, int length) {
        this.id = id;
        this.length = length;
    }

    public String id;
    public int length; 
}
