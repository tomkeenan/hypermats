package street.simulation;

import java.net.URI;

import org.springframework.web.client.RestTemplate;

import street.executor.Executor;
import street.executor.ExecutorFactory;
import street.model.DepartingVehicle;
import street.model.Vehicle;

public class Lane {
    // 500m = 
    // Each "tick" represents 1 minute
    // 1 cell = distance travelled in 1 minute @ 10km/h
    private Vehicle[] positions;

    public Lane(int length) {
        positions = new Vehicle[length];
    }

    public boolean join(Vehicle vehicle) {
        if (positions[0] == null) {
            positions[0] = vehicle;
            return true;
        }
        return false;
    }

    /**
     * Assumption here: the maximum number of vehicles that can leave the street per
     * iteration is 1...
     * @return
     */
    public DepartingVehicle step(int iteration) {
        DepartingVehicle departingVehicle = null;
        int i=positions.length-1;

        // for all positons, check if there is a vehicle,
        // and move it forwards the number of cells based on its speed.
        // auto slow down happens if it cannot move the full number of steps.
        while (i != -1) {
            if (positions[i] != null) {
                Vehicle vehicle = applyAgentAction(positions, i);
                if (vehicle.getSpeed() > 0) {
                    // Move the vehicle forward (speed) spaces
                    int j = 1;

                    while ((j <= vehicle.getSpeed()) && (i+j < positions.length) && (positions[i+j] == null)) {
                        positions[i+j] = positions[i+j-1];
                        positions[i+j].setLocation(i+j);
                        positions[i+j-1] = null;
                        j++;
                    }

                    if (j == vehicle.getSpeed() && (i+j == positions.length)) {
                        // we reached the end of the road
                        positions[i+j-1].remove();
                        departingVehicle = new DepartingVehicle(
                                positions[i+j-1].getAgent().name,
                                transferAgent(positions[i+j-1])
                        );
                        positions[i+j-1]=null;
                    } else if (j <= vehicle.getSpeed() && (positions[i+j] != null)) {
                        // we drove up next to another vehicle, so slow down
                        vehicle.setSpeed(j-1);
                    }
                }
            }
            i--;
        }

        return departingVehicle;
    }

    private URI transferAgent(Vehicle vehicle) {
        System.out.println("Transferring to: " + vehicle.getSimulation().getOut());
        RestTemplate template = new RestTemplate();
        URI location = template.postForLocation(vehicle.getSimulation().getOut(), vehicle.getAgent());
        System.out.println("Now at: " + location);
        return location;
    }

    private Vehicle applyAgentAction(Vehicle[] positions2, int i) {
        System.out.println("[" + positions[i].getAgent().name + "] END: " + positions[i].getAction());

        Executor executor = ExecutorFactory.get(positions[i].getAction().id);
        if (executor != null) {
            executor.execute(positions, i);
        } else {
            throw new RuntimeException("Unknown action: " + positions[i].getAction());
        }

        return positions[i];
    }

    public Object getVehicle(int i) {
        return positions[i] == null ? " ":positions[i].getAgent().name;
    }
}
