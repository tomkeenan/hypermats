package street.simulation;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import hypermats.core.model.AgentDescription;
import hypermats.core.model.Link;
import street.model.DepartingVehicle;
import street.model.StreetConfiguration;
import street.model.StreetDescription;
import street.model.Vehicle;

public class StreetSimulation {
    private StreetConfiguration street;
    private Map<String, Vehicle> vehicles = new HashMap<>();
    private Lane lane;
    private String in;
    private String out;
    
    public StreetSimulation(StreetConfiguration street) {
        this.street = street;
        this.lane = new Lane(street.length);
    }

    public String getUrl(String host) {
        return "http://" + host + "/streets/" + street.id;
    }

    public StreetConfiguration getStreet() {
        return street;
    }

    public String getOut() {
        return out;
    }

    public String getIn() {
        return in;
    }

    public StreetDescription getStreetDescription(String host) {
        StreetDescription description = new StreetDescription(street);
        for (Vehicle vehicle : vehicles.values()) {
            description.vehicles.add(vehicle.getUrl(host));
        }
        description.in.add(new Link(in));
        description.out.add(new Link(out));
        return description;
    }

    public void setIn(String url) {
        this.in = url;
    }

    public void setOut(String url) {
        this.out = url;
    }

    // Simplest implementation
    // uniform speed
    // no collisions
    public Map<String, URI> step(int iteration) {
        Map<String, URI> exitingVehicles = new HashMap<>();
        DepartingVehicle departingVehicle = lane.step(iteration);
        if (departingVehicle != null) {
            exitingVehicles.put(departingVehicle.name, departingVehicle.uri);
        }
        return exitingVehicles;
    }

    public void removeVehicle(Vehicle vehicle) {
        vehicles.remove(vehicle.getAgent().name);
    }

    public Vehicle addVehicle(AgentDescription agent) {
        Vehicle vehicle = new Vehicle(this, agent);
        vehicles.put(agent.name, vehicle);
        lane.join(vehicle);
        return vehicle;
    }

    public String toHTML() {
        StringBuilder builder = new StringBuilder();
        builder.
            append("<style type='text/css'> td { min-width:30px;height:30px;text-align:center;vertical-align:middle; }</style>").
            append("<table border=1><tr><td>IN</td>");
        for (int i=0; i<street.length; i++) {
            builder.
                append("<td>").
                append(i).
                append("</td>");
        }
        builder.append("<td>OUT</td></tr><tr><td>").
            append(in).
            append("</td>");
        for (int i=0; i<street.length; i++) {
            builder.
                append("<td>").
                append(lane.getVehicle(i)).
                append("</td>");
        }
        builder
            .append("<td>")
            .append(out)
            .append("</td></tr></table>");

        return builder.toString();
    }
}
