package street.controller;

import java.net.InetAddress;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import hypermats.core.model.Action;
import hypermats.core.model.AgentDescription;
import hypermats.core.model.Iteration;
import hypermats.core.model.Link;
import street.model.EnvironmentState;
import street.model.StreetConfiguration;
import street.model.StreetDescription;
import street.model.Vehicle;
import street.model.VehicleDescription;
import street.service.StreetService;
import street.service.VehicleService;
import street.simulation.StreetSimulation;

@RestController
public class StreetController {
    private Iteration iteration;
    private Map<String, URI> redirects = new HashMap<>();

    @Autowired private StreetService streetService;
    @Autowired private VehicleService vehicleService;

    @Value("${server.port}")
    private int port;
    
    @GetMapping("/streets/iteration")
    @ResponseStatus(HttpStatus.OK)
    public Iteration getIteration() {
        return iteration;
    }

    @PutMapping("/streets/iteration")
    @CrossOrigin
    public void setIteration(@RequestBody Iteration iteration) {
        this.iteration = iteration;
        System.out.println("Iteration: " + iteration.getIteration());

        // Step streets...
        for (StreetSimulation simulation : streetService.getSimulations()) {
            Map<String, URI> exitingVehicles = simulation.step(iteration.getIteration());
            for (Entry<String, URI> entry : exitingVehicles.entrySet()) {
                System.out.println("Removing: " + entry.getKey());
                vehicleService.remove(entry.getKey());
                redirects.put(entry.getKey(), entry.getValue());
            }
        }

        // Send updated states to vehicles...
        RestTemplate template = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        for (Vehicle vehicle : vehicleService.getVehicles()) {
            try {
                EnvironmentState state = new EnvironmentState(vehicle.getState(getHost()));
                HttpEntity<EnvironmentState> entity = new HttpEntity<>(state, headers);
                template.put(vehicle.getAgent().webhook, entity);
            } catch (Exception e) {
                System.out.println("Could not update: " + vehicle.getAgent().name);
            }
        }
    }
        

    @PutMapping("/vehicle/{name}/action")
    public ResponseEntity<Action> setAction(@PathVariable String name, @RequestBody Action action) {
        Vehicle vehicle = vehicleService.getVehicle(name);
        if (vehicle == null) {
            URI uri = redirects.get(name);
            if (uri == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }
            return ResponseEntity
                    .status(HttpStatus.TEMPORARY_REDIRECT)
                    .header(HttpHeaders.LOCATION, uri.toASCIIString())
                    .build();
        }
        vehicle.setAction(action);
        return ResponseEntity.status(HttpStatus.OK).body(action);
    }
    
    @PatchMapping("/vehicle/{name}")
    public ResponseEntity<VehicleDescription> updateAction(@PathVariable String name, @RequestBody VehicleDescription description) {
        if (description.perception != null || description.action == null) {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(description);
        }

        Vehicle vehicle = vehicleService.getVehicle(name);
        if (vehicle == null) {
            URI uri = redirects.get(name);
            if (uri == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }
            return ResponseEntity
                    .status(HttpStatus.TEMPORARY_REDIRECT)
                    .header(HttpHeaders.LOCATION, uri.toASCIIString())
                    .build();
        }
        vehicle.setAction(description.action);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
    
    @GetMapping("/vehicle/{name}")
    public ResponseEntity<VehicleDescription> getVehicle(@PathVariable String name) {
        Vehicle vehicle = vehicleService.getVehicle(name);
        if (vehicle == null) {
            URI uri = redirects.get(name);
            if (uri == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }
            return ResponseEntity
                    .status(HttpStatus.TEMPORARY_REDIRECT)
                    .header(HttpHeaders.LOCATION, uri.toASCIIString())
                    .build();
        }

        try {
            VehicleDescription description = new VehicleDescription(vehicle.getState(getHost()), vehicle.getAction());
            return ResponseEntity.status(HttpStatus.OK).body(description);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping("/streets")
    public ResponseEntity<String> addStreet(@RequestBody StreetConfiguration street) {
        if (streetService.hasStreet(street.id)) {
            System.out.println("WARNING: Creating (Replacing) Duplicate Street: " + street.id);
            // TODO: Uncomment when not testing
            // return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }

        StreetSimulation simulation = streetService.addStreet(street);
        
        try {
            return ResponseEntity.
                        status(HttpStatus.CREATED).
                        header(HttpHeaders.LOCATION, simulation.getUrl(getHost())).
                        build();
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping(value="/streets", produces="application/json")
    public ResponseEntity<LinkedList<String>> getStreets() {
        LinkedList<String> list = new LinkedList<>();
        for (StreetSimulation simulation : streetService.getSimulations()) {
            try {
                list.add(simulation.getUrl(getHost()));
            } catch (UnknownHostException e) {
                e.printStackTrace();
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
        }
        return ResponseEntity.status(HttpStatus.OK).body(list);
    }

    @GetMapping(value="/streets", produces="text/html")
    public ResponseEntity<String> getStreetsHtml() {
        StringBuilder builder = new StringBuilder();
        builder.append("<TABLE>");
        for (StreetSimulation simulation : streetService.getSimulations()) {
            builder
                .append("<TR><TD>")
                .append(simulation.getStreet().id)
                .append("</TD><TD>")
                .append(simulation.toHTML())
                .append("</TD></TR>");
        }
        builder.append("</TABLE>");

        return ResponseEntity.status(HttpStatus.OK).body(builder.toString());
    }

    @GetMapping(value="/streets/{id}", produces="text/html")
    public ResponseEntity<String> getStreetHtml(@PathVariable String id) {
        StreetSimulation simulation = streetService.getSimulation(id);
        return ResponseEntity.status(HttpStatus.OK).body(simulation.toHTML());
    }

    @GetMapping(value="/streets/{id}/in", produces="application/json")
    public ResponseEntity<LinkedList<Link>> getStreetIn(@PathVariable String id) {
        StreetSimulation simulation = streetService.getSimulation(id);
        if (simulation == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        try {
            StreetDescription description = simulation.getStreetDescription(getHost());
            return ResponseEntity.status(HttpStatus.OK).body(description.in);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping(value="/streets/{id}/out", produces="application/json")
    public ResponseEntity<LinkedList<Link>> getStreetOut(@PathVariable String id) {
        StreetSimulation simulation = streetService.getSimulation(id);
        if (simulation == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        try {
            StreetDescription description = simulation.getStreetDescription(getHost());
            return ResponseEntity.status(HttpStatus.OK).body(description.out);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping(value="/streets/{id}/in/{index}")
    public ResponseEntity<String> linkInJunction(@PathVariable String id, @PathVariable int index, @RequestBody Link link) {
        StreetSimulation simulation = streetService.getSimulation(id);
        if (index != 0) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        simulation.setIn(link.url);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PostMapping(value="/streets/{id}/in/{index}")
    public ResponseEntity<String> receiveAgent(@PathVariable String id, @PathVariable int index, @RequestBody AgentDescription agentDescription) {
        StreetSimulation simulation = streetService.getSimulation(id);
        if (index != 0) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        Vehicle vehicle = simulation.addVehicle(agentDescription);
        vehicleService.add(vehicle);
        try {
            return ResponseEntity.
                        status(HttpStatus.CREATED).
                        header(HttpHeaders.LOCATION, vehicle.getUrl(getHost())).
                        build();
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping(value="/streets/{id}/out/{index}")
    public ResponseEntity<String> linkOutJunction(@PathVariable String id, @PathVariable int index, @RequestBody Link link) {
        StreetSimulation simulation = streetService.getSimulation(id);
        if (index != 0) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        simulation.setOut(link.url);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping(value="/streets/{id}", produces="application/json")
    public ResponseEntity<StreetDescription> getStreet(@PathVariable String id) {
        StreetSimulation simulation = streetService.getSimulation(id);
        try {
            return ResponseEntity.status(HttpStatus.OK).body(simulation.getStreetDescription(getHost()));
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    private String getHost() throws UnknownHostException {
        return InetAddress.getLocalHost().getHostAddress() + ":" + port;
    }
}
