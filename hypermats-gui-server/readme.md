# GUI Server

## Installing

Dependencies: npm 9.3.0

To install run: `npm install` 

## Running

Run: `npm start`

Then start the GUI Client.

The client will register with the server, then to display the agent, send curl requests to the server:

``
curl -X POST http://localhost:9898/agent \
   -H 'Content-Type: application/json' \
   -d '{"agent":"agentZero","lat":"53.3058","lon":"-6.2220"}'

``

Modify the "lon" or "lat" co-ordinates to see the agent move.