package street.controller;

import java.net.InetAddress;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.LinkedList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;

import hypermats.core.model.AgentDescription;
import hypermats.core.model.Iteration;
import hypermats.core.model.Link;
import hypermats.core.model.VehicleType;
import street.model.StreetConfiguration;
import street.model.Vehicle;
import street.model.VehicleDescription;
import street.service.RedirectsService;
import street.service.StreetService;
import street.service.VehicleService;
import street.service.VehicleTypeService;
import street.simulation.StreetSimulation;

@RestController
public class StreetController {
    private Iteration iteration;

    @Autowired
    private StreetService streetService;
    @Autowired
    private RedirectsService redirectsService;
    @Autowired
    private VehicleService vehicleService;
    @Autowired
    private VehicleTypeService vehicleTypeService;

    @Value("${server.port}")
    private int port;

    @GetMapping("/streets/iteration")
    @ResponseStatus(HttpStatus.OK)
    public Iteration getIteration() {
        return iteration;
    }

    @PutMapping("/streets/iteration")
    @CrossOrigin
    public void setIteration(@RequestBody Iteration iteration) {
        this.iteration = iteration;
        System.out.println("Iteration: " + iteration.getIteration());

        // Step streets...
        for (StreetSimulation simulation : streetService.getSimulations()) {
            for (Vehicle vehicle : simulation.step(iteration.getIteration())) {
                System.out.println("Removing: " + vehicle.name);
                vehicleService.remove(vehicle.name);

                System.out.println("Transferring to: " + vehicle.getSimulation().getOut());
                try {
                    RestTemplate template = new RestTemplate();
                    URI location = template.postForLocation(vehicle.getSimulation().getOut(), vehicle.toJson());
                    System.out.println("Now at: " + location);
                    redirectsService.put(vehicle.name, location);
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
        }

        // Send updated states to vehicles...
        RestTemplate template = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        for (Vehicle vehicle : vehicleService.getVehicles()) {
            try {
                HttpEntity<JsonNode> entity = new HttpEntity<>(vehicle.toJson(), headers);
                template.put(vehicle.agent.getAgent().webhook, entity);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Could not update: " + vehicle.name);
            }
        }
    }

    @PostMapping("/streets")
    public ResponseEntity<String> addStreet(@RequestBody StreetConfiguration street) {
        if (streetService.hasStreet(street.id)) {
            System.out.println("WARNING: Creating (Replacing) Duplicate Street: " + street.id);
            // TODO: Uncomment when not testing
            // return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }

        StreetSimulation simulation = null;

        try {
            simulation = streetService.addStreet(street, getHost());
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

        return ResponseEntity.status(HttpStatus.CREATED).header(HttpHeaders.LOCATION, simulation.getUrl()).build();
    }

    @GetMapping(value = "/streets", produces = "application/json")
    public ResponseEntity<LinkedList<String>> getStreets() {
        LinkedList<String> list = new LinkedList<>();
        for (StreetSimulation simulation : streetService.getSimulations()) {
            list.add(simulation.getUrl());
        }
        return ResponseEntity.status(HttpStatus.OK).body(list);
    }

    @GetMapping(value = "/streets", produces = "text/html")
    public ResponseEntity<String> getStreetsHtml() {
        StringBuilder builder = new StringBuilder();
        builder.append("<TABLE>");
        for (StreetSimulation simulation : streetService.getSimulations()) {
            builder
                .append("<TR><TD>")
                .append(simulation.getStreet().id)
                .append("</TD><TD>")
                .append(simulation.toHTML())
                .append("</TD></TR>");
        }
        builder.append("</TABLE>");

        return ResponseEntity.status(HttpStatus.OK).body(builder.toString());
    }

    @GetMapping(value = "/streets/{id}", produces = "text/html")
    public ResponseEntity<String> getStreetHtml(@PathVariable String id) {
        StreetSimulation simulation = streetService.getSimulation(id);
        return ResponseEntity.status(HttpStatus.OK).body(simulation.toHTML());
    }

    @GetMapping(value = "/streets/{id}", produces = "application/json")
    public ResponseEntity<JsonNode> getStreet(@PathVariable String id) {
        StreetSimulation simulation = streetService.getSimulation(id);
        if (simulation == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.status(HttpStatus.OK).body(simulation.toJson());
    }

    @GetMapping(value = "/streets/{id}/in", produces = "application/json")
    public ResponseEntity<LinkedList<Link>> getStreetIn(@PathVariable String id) {
        StreetSimulation simulation = streetService.getSimulation(id);
        if (simulation == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        return ResponseEntity.status(HttpStatus.OK).body(simulation.getStreetDescription().in);
    }

    @GetMapping(value = "/streets/{id}/out", produces = "application/json")
    public ResponseEntity<LinkedList<Link>> getStreetOut(@PathVariable String id) {
        StreetSimulation simulation = streetService.getSimulation(id);
        if (simulation == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        return ResponseEntity.status(HttpStatus.OK).body(simulation.getStreetDescription().out);
    }

    @PutMapping(value = "/streets/{id}/in/{index}")
    public ResponseEntity<String> linkInJunction(@PathVariable String id, @PathVariable int index,
            @RequestBody Link link) {
        StreetSimulation simulation = streetService.getSimulation(id);
        if (index != 0) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        simulation.setIn(link.url);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping(value = "/streets/{id}/in/{index}")
    public ResponseEntity<String> getInJunction(@PathVariable String id, @PathVariable int index) {
        StreetSimulation simulation = streetService.getSimulation(id);
        if (index != 0) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        return ResponseEntity.status(HttpStatus.OK).body(simulation.getIn());
    }

    @PostMapping(value = "/streets/{id}/in/{index}")
    public ResponseEntity<String> receiveAgent(@PathVariable String id, @PathVariable int index,
            @RequestBody JsonNode node) {
        System.out.println("-------------------------------------------------------------");
        System.out.println(node.toPrettyString());
        System.out.println("-------------------------------------------------------------");
        StreetSimulation simulation = streetService.getSimulation(id);
        if (index != 0) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        // If it is not a vehicle object, fail
        if (!node.get("@type").asText().equals("Vehicle")) {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).build();
        }

        JsonNode typeNode = node.get("type");
        JsonNode agent = node.get("agent");
        if (typeNode == null || agent == null || !node.has("name")) {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).build();
        }
        Vehicle vehicle = null;
        try {
            VehicleType vehicleType = vehicleTypeService.fromJson(typeNode);
            VehicleDescription vehicleDescription = new VehicleDescription();
            vehicleDescription.name = node.get("name").asText();
            vehicleDescription.agentDescription = new AgentDescription();
            vehicleDescription.agentDescription.name = agent.get("name").asText();
            vehicleDescription.agentDescription.webhook = agent.get("webhook").asText();

            vehicle = vehicleService.add(vehicleDescription, vehicleType, getHost());
            vehicleService.initialize(vehicle, node);
            simulation.addVehicle(vehicle);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

        return ResponseEntity.status(HttpStatus.CREATED).header(HttpHeaders.LOCATION, vehicle.url).build();
    }

    @PutMapping(value = "/streets/{id}/out/{index}")
    public ResponseEntity<String> linkOutJunction(@PathVariable String id, @PathVariable int index,
            @RequestBody Link link) {
        StreetSimulation simulation = streetService.getSimulation(id);
        if (index != 0) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        simulation.setOut(link.url);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    private String getHost() throws UnknownHostException {
        return InetAddress.getLocalHost().getHostAddress() + ":" + port;
    }
}
